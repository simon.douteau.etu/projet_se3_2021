CC = gcc
CFLAGS = -Wall -Wextra -g
EXEC = exec
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)

all : $(EXEC)

%.o : %.c
	$(CC) -o $@ -c $< $(CFLAGS)

$(EXEC) : $(OBJ)
	$(CC) -o $@ $^
