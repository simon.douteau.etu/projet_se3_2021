Utilisation du programme de Rémi BRACHOT et Simon DOUTEAU

Pour utiliser correctement le programme, vous devez dans un premier temps
creer un <fichier.txt>, dans le dossier data, dans lequel vous donnerez chacune 
des requêtes que vous souhaitez. Chaque ligne de ce fichier devra correspondre 
à une seule requête en commencant en premier lieu par le nom de celle-ci suivi 
par les différents paramètres que la requête nécessite.

Dans un second temps, vous devez compiler le fichier intitulé projet_fini, à l'aide de la commande <make>. Pour ensuite éxecuter le programme et ainsi, afficher le résultats des requêtes que vous avez mis en amond dans le <fichier.txt>, vous devez utiliser la commande : ./exec data/fichier.txt

/*******************************************************/

Voici la liste des requêtes fonctionnelles que vous pouvez utiliser avec leur nom, leurs paramètres (obligatoires <..> et optionnels [..]) et ce qu'elles font :

show-airports <airline_id> : affiche tous les aéroports depuis lesquels la compagnie aérienne `<airline_id>` opère des vols.

show-airlines <port_id> : affiche les compagnies aériens qui ont des vols qui partent de l'aéroport `<port_id>`.

show-flights <port_id> <date> [<time>] [limit=<xx>] : affiche les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols.

most-delayed-flights : donne les 5 vols qui ont subis les plus longs retards à l'arrivée.

most-delayed-airlines : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards.

delayed-airline <airline_id> : donne le retard moyen de la compagnie aérienne passée en paramètre.

most-delayed-airlines-at-airport <airport_id> : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre.

changed-flights <date> : les vols annulés ou déviés à la date <date> (format M-D).

avg-flight-duration <port_id> <port_id> : calcule le temps de vol moyen entre deux aéroports.

find-itinerary <port_id> <port_id> <date> [<time>] [limit=<xx>] : trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnel, requête peut être limité à `xx` propositions, il peut y avoir des escales).

quit : quitte

/*******************************************************/

Ci dessous un exemple de fichier.txt :

/*******************************************************/

show-airports HA
show-airlines LAX
show-flights ATL 2-26
show-flights ATL 4-17 1600 limit=5
avg-flight-duration LAX JFK
find-itinerary PHX SAN 12-15
most-delayed-flights
most-delayed-airlines
changed-flights 5-15
find-itinerary DEN MCI 2-15 1030 limit=4
find-itinerary SAN JFK 6-15 1030 IAD 6-19 1215 SLC 6-21 SAN 6-25
delayed-airline US

/*******************************************************/