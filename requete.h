#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define NB_JOUR 400
#define NB_AIRLINE 100
#define NB_AIRPORTS 400
#define NOMBRE_COMPAGNIE 14
#define NOMBRE_AEROPORT 322
#define N 100

struct date{
  int month;
  int day;
  int weekday;
};

struct flight{
  struct date day_fly;
  char airline[3];
  char org_air[4];
  char dest_air[4];
  int sche_dep;
  float dep_delay;
  float air_time;
  int dist;
  int sched_arr;
  float arr_delay;
  int diverted;
  int cancelled;
} Flight;

struct pt_flight{
  struct flight vool;
  struct pt_flight* nextdate;
  struct pt_flight* nextcompany;
  struct pt_flight* nextairportdep;
  struct pt_flight* nextairportarr;
};

struct aeroports{
  char code_iata[4];
  char nom_aero[100];
  char ville[50];
  char etat[25];
  char pays[15];
  float latitude;
  float longitude;
};

struct airline{
  char iata_code[3];
  char name[150];
};

struct hachagedate{
  struct pt_flight* jourmois[NB_JOUR];
};

struct hachageairline{
  struct pt_flight* line[NB_AIRLINE];
};

struct hachageairportsdep{
  struct pt_flight* airportsdep[NB_AIRPORTS];
};

struct hachageairportsarr{
  struct pt_flight* airportsarr[NB_AIRPORTS];
};

void ajoutdate(struct pt_flight** ptvol, struct hachagedate* pthdate);
void ajoutairline(struct pt_flight** ptvol, struct hachageairline* pthairline);
void ajoutairportdep(struct pt_flight** ptvol, struct hachageairportsdep* pthairportdep);
void ajoutairportarr(struct pt_flight** ptvol, struct hachageairportsarr* pthairportarr);
void remplir( FILE* f, struct hachagedate* pthdate, struct hachageairline* pthairline, struct hachageairportsdep* pthairportdep, struct hachageairportsarr* pthairportarr);
void remplirairports( FILE* f, struct aeroports aero[] );
void remplirligneaero(FILE* f, struct airline air[]);


void show_airports(char ligne[], struct hachageairline hairline, struct aeroports stockaeroport[]);
void show_airline(char dep[], struct hachageairportsdep hairportdep, struct airline stockairline[]);
void show_flights(struct hachagedate h, char orig_air[4], struct date d);
void most_delayed_flights();
int delayed_airline(struct hachageairline h, char airlines[1]);
void most_delayed_airlines(struct hachageairline h);
void most_delayed_airlines_at_airport();
void changed_flights(struct hachagedate h, struct date d);
void avg_flights_duration();
void find_itinerary(struct hachagedate h, struct date d, char airport1[3], char airport2[3]);
void find_multicity_itinerary();
void quit();
