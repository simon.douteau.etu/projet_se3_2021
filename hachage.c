#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "requete.h"

//On ajoute les listes de vols au bonne indice dans la table de hachage correspondant
  void ajoutdate(struct pt_flight** ptvol, struct hachagedate* pthdate){

  int hashdata = (((*ptvol)->vool.day_fly.month)-1)*31 + ((*ptvol)->vool.day_fly.day);

  (*ptvol)->nextdate = pthdate->jourmois[hashdata];
  pthdate->jourmois[hashdata] = *ptvol;

  return;
}

void ajoutairline(struct pt_flight** ptvol, struct hachageairline* pthairline){

  int hashairline = (int)((*ptvol)->vool.airline[0]) + (int)((*ptvol)->vool.airline[1]) -96;

  

  (*ptvol)->nextcompany = pthairline->line[hashairline];
  pthairline->line[hashairline] = *ptvol;

  return;
}

void ajoutairportdep(struct pt_flight** ptvol, struct hachageairportsdep* pthairportdep){

  int hashairportdep = (int)((*ptvol)->vool.org_air[0])*3 + (int)((*ptvol)->vool.org_air[1])*2 + (int)((*ptvol)->vool.org_air[2])*1 -390;

  (*ptvol)->nextairportdep = pthairportdep->airportsdep[hashairportdep];
  pthairportdep->airportsdep[hashairportdep] = *ptvol;

  return;
}

void ajoutairportarr(struct pt_flight** ptvol, struct hachageairportsarr* pthairportarr){

  int hashairportarr = (int)((*ptvol)->vool.dest_air[0])*3 + (int)((*ptvol)->vool.dest_air[1])*2 + (int)((*ptvol)->vool.dest_air[2])*1 -390;

  (*ptvol)->nextairportarr = pthairportarr->airportsarr[hashairportarr];
  pthairportarr->airportsarr[hashairportarr] = *ptvol;

  return;
}

//On rempli les tables de hachages avec les vols
void remplir( FILE* f, struct hachagedate* pthdate, struct hachageairline* pthairline, struct hachageairportsdep* pthairportdep, struct hachageairportsarr* pthairportarr){
  size_t n = 500;
  char* ptligne = malloc(sizeof(char)* n);

  getline(&ptligne,&n,f);
  while(getline(&ptligne,&n,f) != EOF){

      struct pt_flight* vol  = malloc(sizeof(struct pt_flight));
      vol->vool.day_fly.month = atoi( strsep(&ptligne,","));
      vol->vool.day_fly.day = atoi ( strsep(&ptligne,","));
      vol->vool.day_fly.weekday  = atoi (strsep(&ptligne,","));
      strcpy( vol->vool.airline, strsep(&ptligne,","));
      strcpy( vol->vool.org_air, strsep(&ptligne,","));
      strcpy( vol->vool.dest_air, strsep(&ptligne,","));
      vol->vool.sche_dep = atoi( strsep(&ptligne,","));
      vol->vool.dep_delay = atof( strsep(&ptligne,","));
      vol->vool.air_time = atof( strsep(&ptligne,","));
      vol->vool.dist = atoi( strsep(&ptligne,","));
      vol->vool.sched_arr = atoi(strsep(&ptligne,","));
      vol->vool.arr_delay = atof(strsep(&ptligne,","));
      vol->vool.diverted = atoi(strsep(&ptligne,","));
      vol->vool.cancelled = atoi(strsep(&ptligne,","));
      vol->nextdate = NULL;
      vol->nextcompany = NULL;
      vol->nextairportdep = NULL;

      ajoutdate(&vol, pthdate);
      ajoutairline(&vol, pthairline);
      ajoutairportdep(&vol, pthairportdep);
      ajoutairportarr(&vol, pthairportarr);

  }

  return;
}

void remplirairports( FILE* f, struct aeroports aero[]){

  size_t n = 1000;
  char* ptligne = malloc(sizeof(char) * n);
  int i=0;

  getline(&ptligne,&n,f);
  while(getline(&ptligne, &n, f)!= EOF){

    strcpy( aero[i].code_iata, strsep(&ptligne,","));

    strcpy( aero[i].nom_aero, strsep(&ptligne,","));

    strcpy( aero[i].ville, strsep(&ptligne,","));

    strcpy( aero[i].etat, strsep(&ptligne,","));

    strcpy( aero[i].pays, strsep(&ptligne,","));

    aero[i].latitude = atof( strsep(&ptligne,","));

    aero[i].longitude = atof( strsep(&ptligne,","));


    //printf("%s, %s, %s, %s, %s, %f, %f\n",aero[i].code_iata, aero[i].nom_aero, aero[i].ville, aero[i].etat, aero[i].pays, aero[i].latitude, aero[i].longitude);
    
    i++;
    
  }
  return;
}

void remplirligneaero(FILE* f, struct airline air[]){

  size_t n = 1000;
  char* ptligne = malloc(sizeof(char) * n);
  int i=0;

  getline(&ptligne,&n,f);
  while(getline(&ptligne, &n, f)!= EOF){

    strcpy( air[i].iata_code, strsep(&ptligne,","));

    strcpy( air[i].name, strsep(&ptligne,"\n"));

    //printf("%s, %s\n", air[i].iata_code, air[i].name);

    i++;

  }

  return;

}

