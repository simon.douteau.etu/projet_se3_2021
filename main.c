#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "requete.h"

//Petite fonction qui permet de faire une pause dans l'execution des requetes
void pause(){
    printf("\n"); 
    char c = 0;
    printf("Press c to continue\n");
    while ((c != 'c')) c = getchar();//Tant que l'utilisateur n'a pas appuyé sur c + Entrée

    return ;
} 

int main(int argc, char* argv[]){
  //Initialisation des structures de hachage
  struct hachagedate hdate;
  struct hachageairline hairline;
  struct hachageairportsdep hairportdep;
  struct hachageairportsarr hairportarr;
  struct aeroports stockaeroport[NOMBRE_AEROPORT];
  struct airline stockairline[NOMBRE_COMPAGNIE];

  //initialisation des pointeurs
  for(int i = 0; i<NB_JOUR ; i++ )
    hdate.jourmois[i] = NULL;

  for(int k =0; k<NB_AIRLINE ; k++)
    hairline.line[k] = NULL;

  for (int j = 0; j<NB_AIRPORTS; j++){
    hairportdep.airportsdep[j] = NULL;
    hairportarr.airportsarr[j] = NULL;
  }

  //On vérifie qu'un fichier de requete a été donné en paramètre du main
  if(argc < 2){
    printf("Aucun fichier de requêtes donnée\n");
    exit(EXIT_FAILURE);
  }

  //pointage des fichiers
  FILE* fichier =fopen("data/flights.csv","r");
  FILE* ligneaero = fopen("data/airlines.csv","r");
  FILE* airports = fopen("data/airports.csv","r");
  FILE* fp = fopen(argv[1], "r");

  //On vérifie que les fichiers se sont ouverts correctements
  if(fp == NULL || fichier == NULL || ligneaero == NULL || airports == NULL){
    printf("Le fichier ne s'est pas correctement ouvert\n");
    exit(EXIT_FAILURE);
  }
  
  //Appel des fonctions pour remplir la table
  remplir(fichier, &hdate, &hairline, &hairportdep, &hairportarr);
  remplirairports(airports, stockaeroport);
  remplirligneaero(ligneaero, stockairline);

  //Récupération de la requete et séparation des mots pour récupérer les pramètres
  //Initialisation des varibles
  char nom_requete[N];
  int cpt = 0;
  size_t n = 500;
  char* ptline = malloc(sizeof(char) * n);
  const char* delim = " ";
  while(getline(&ptline, &n, fp) != EOF){//Tant qu'on arrive pas à la fin du fichier de requetes
    strcpy(nom_requete, strsep(&ptline,delim));//On récupère le nom de la requete
    //On execute les fonctions correspondantes au nom de la requete en récupérant les paramètres
    if( strcmp("show-airports",nom_requete) == 0 ){
       char argument[3];
       strcpy(argument, strsep(&ptline,"\n"));
       show_airports(argument, hairline, stockaeroport);
    }

    if( strcmp("show-flights",nom_requete) == 0 ){
      char argumentaeroport[4];
      struct date argumentdate;
      strcpy(argumentaeroport, strsep(&ptline," "));
      argumentdate.month = atoi( strsep(&ptline,"-"));
      argumentdate.day = atoi( strsep(&ptline,"\n"));
      argumentdate.weekday = 0;
      show_flights(hdate, argumentaeroport, argumentdate);
    }
    
    if( strcmp("show-airlines",nom_requete) == 0 ){
      char argument[4];
      strcpy(argument, strsep(&ptline,"\n"));
      show_airline(argument, hairportdep, stockairline);
    }
    
    if( strcmp("avg-flight-duration",nom_requete) == 0 ){
        char aero1[4];
        char aero2[4];
        strcpy(aero1, strsep(&ptline," "));
        strcpy(aero2, strsep(&ptline,"\n"));
        avg_flights_duration(hairportdep, aero1, aero2);
    }
    
    if( strcmp("find-itinerary",nom_requete) == 0 ){
        char aero1[4];
        char aero2[4];
        struct date argumentdate;
        strcpy(aero1, strsep(&ptline," "));
        strcpy(aero2, strsep(&ptline," "));
        argumentdate.month = atoi( strsep(&ptline,"-"));
        argumentdate.day = atoi( strsep(&ptline," ,\n"));
        argumentdate.weekday = 0;
        find_itinerary(hdate, argumentdate, aero1, aero2);
    }
    
    if( strcmp("most-delayed-flights\n",nom_requete) == 0 ){
	    most_delayed_flights(hairline);
    }
    
    if( strcmp("most-delayed-airlines\n",nom_requete) == 0 ){
	    most_delayed_airlines(hairline);
    }

    if( strcmp("changed-flights",nom_requete) == 0 ){
        struct date argumentdate;
        argumentdate.month = atoi( strsep(&ptline,"-"));
        argumentdate.day = atoi( strsep(&ptline," ,\n"));
        argumentdate.weekday = 0;
        changed_flights(hdate, argumentdate);
    }
    
    if( strcmp("delayed-airline",nom_requete) == 0 ){
        char argument[3];
        strcpy(argument, strsep(&ptline,"\n"));
        int result = delayed_airline(hairline, argument);
	printf("La compagnie %s a un retard moyen de %d min\n", argument, result);
    }
    
    //On fait une pause après chaque requêtes faites
    pause();
    cpt++;
  }
  //On libère la mémoire de toutes les tables de hachages

  
  //On ferme les fichiers
  fclose(fp);
  fclose(ligneaero);
  fclose(airports);
  fclose(fichier);
}
