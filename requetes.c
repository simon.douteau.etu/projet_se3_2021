#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "requete.h"

//requete 1 : affiche tous les aéroports depuis lesquels la compagnie aérienne passé en parametre opère des vols
void show_airports(char ligne[], struct hachageairline hairline, struct aeroports stockaeroport[]){

  int hashairline = (int)(ligne[0]) + (int)(ligne[1]) -96;
  int cpt;

  struct pt_flight* tmp = hairline.line[hashairline];
  printf("\n");
  printf("Les aéroports depuis lesquels la compagnie aérienne %s opère des vols sont : \n", ligne);
  printf("\n");
  while (tmp != NULL){
    cpt = 0;
    while(strcmp(tmp->vool.org_air, stockaeroport[cpt].code_iata) != 0 )
      cpt++;
    printf("%s, %s, %s, %s, %s, %f, %f\n",stockaeroport[cpt].code_iata, stockaeroport[cpt].nom_aero, stockaeroport[cpt].ville, stockaeroport[cpt].etat, stockaeroport[cpt].pays, stockaeroport[cpt].latitude, stockaeroport[cpt].longitude);
    //moche, il faudrait créé une liste chainée qu'on remplit si l'élément est pas encore dedans (liste chainée de pointeur ?)

    tmp = tmp->nextcompany;
    }
}
    
//requete 2 : affiche les compagnies aériens qui ont des vols qui partent de l'aéroport passé en paramètre
void show_airline(char dep[], struct hachageairportsdep hairportdep, struct airline stockairline[]){

  int hashairportdep = (int)(dep[0])*3 + (int)(dep[1])*2 + (int)(dep[2])*1 -390;
  int cpt;

  struct pt_flight* tmp = hairportdep.airportsdep[hashairportdep];
  printf("\n");
  printf("Les compagnies qui ont des vols qui partent de l'aéroport %s sont : \n", dep);
  while (tmp != NULL){
    cpt = 0;
    while(strcmp(tmp->vool.airline, stockairline[cpt].iata_code) != 0)
      cpt++;
    printf("%s, %s\n", stockairline[cpt].iata_code, stockairline[cpt].name);
    //moche comme la requete 1 faut améliorer
    tmp = tmp->nextairportdep;
  }
}

//requete 3 : affiche les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols
void show_flights(struct hachagedate h, char orig_air[], struct date d){
    int jour = ((d.month)-1)*31 +(d.day); //Récupération de l'indice de la table de hachage correspondant à la date
    struct pt_flight* vol = h.jourmois[jour]; 
    printf("\n");
    printf("Les vols qui partent de l'aéroport %s, à la date %d/%d sont : \n", orig_air, d.day, d.month);
    while(vol->nextdate != NULL){//Tant qu'on a pas parcouru toute le liste chainée
        if(strcmp(vol->vool.org_air, orig_air) == 0){//Si on tombe sur qui part de l'aeroport orig_air alors on affiche le vol
            printf("%s, %s, %s, %d, %f, %f, %d, %d, %f, %d, %d\n", vol->vool.org_air, vol->vool.airline, vol->vool.dest_air, vol->vool.sche_dep, vol->vool.dep_delay, vol->vool.air_time, vol->vool.dist, vol->vool.sched_arr, vol->vool.arr_delay,vol->vool.diverted, vol->vool.cancelled);
            vol = vol->nextdate;
        }
        else{//Sinon on continue de parcourir la liste
            vol = vol->nextdate;
        }
    }
}

//Requete 4 : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
void most_delayed_flights(struct hachageairline h){
    struct pt_flight* vol2[5]; //Initialisation des variables
    struct pt_flight* tmp;
    int retard[5] = {0,0,0,0,0};
    int max = 0, tmp2 = 0;
    for(int i = 0 ; i < NB_AIRLINE ; i++){//On parcourt toute la table de hachage
        if((h.line[i]) != NULL){//Si la case pointe sur une liste 
            struct pt_flight* vol = h.line[i];//Alors on initialise le pointeur vers la liste
            while(vol != NULL){//On parcourt la liste
                max = (vol->vool.arr_delay);//
                if(retard[0] <= max){//Si le retard est superieur au plus petit retard trouvé jusqu'ici 
                    retard[0] = max;//Alors on le place dans le tableau
                    vol2[0]= vol;//On place le vol correspondant dans le tableau à l'indice correspondant
                }
                for(int j = 0 ; j < 4 ; j++){//Tri à bulle pour classer les vols avec dans l'ordre de sorte à toujours avoir le retard du 5ème vol toujours plus petit que les suivants
                    for(int k = 0 ; k < 4 ; k++){
                        if(retard[k] >= retard[k+1]){
                            tmp2 = retard[k];
                            tmp = vol2[k];
                            retard[k] = retard[k+1];
                            vol2[k] = vol2[k+1];
                            retard[k+1] = tmp2;
                            vol2[k+1] = tmp;
                        }
                    }
                }
                vol = vol->nextcompany; 
            }
        }
    }

    printf("\n");
    printf("Les 5 vols qui ont subi le plus de retard sont : \n");
    printf("\n");
    for (int i = 0; i<5; i++){
      printf("%s, %s, %s, %d, %f, %f, %d, %d, %f, %d, %d avec %d min de retard\n", vol2[i]->vool.airline, vol2[i]->vool.org_air, vol2[i]->vool.dest_air, vol2[i]->vool.sche_dep, vol2[i]->vool.dep_delay, vol2[i]->vool.air_time, vol2[i]->vool.dist, vol2[i]->vool.sched_arr, vol2[i]->vool.arr_delay, vol2[i]->vool.cancelled, vol2[i]->vool.diverted, retard[i]);
    }
}

//Requete 5 : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
void most_delayed_airlines(struct hachageairline h){
    char tableau[5][3] = {"  ","  ","  ","  ","  "};//Initialisation des variables
    char airline[2], tmp[2];
    int retard[5] = {0,0,0,0,0};
    int moyenne = 0, tmp2 = 0;
    for(int i = 0 ; i < 100 ; i++){//On parcourt toute la table de hachage
        if((h.line[i]) != NULL){//Si la case pointe sur une liste
            strcpy(airline, ((h.line[i])->vool.airline));
            moyenne = delayed_airline(h, airline);//On regarde le retard moyen de la compagnie aerienne       
            if(retard[0] <= moyenne){//Si le retard moyen de la compagnie est superieur au plus petit retard moyen trouvé jusqu'ici
                retard[0] = moyenne;//Alors on le palce dans le tableau en premiere position
                strcpy(tableau[0],airline); 
            }
            for(int j = 0 ; j <= 4 ; j++){//Tri à bulle pour trier le premier élément par rapport aux suivants
                for(int k = 0 ; k < 5 ; k++){
                    if(retard[k] >= retard[k+1]){
                        tmp2 = retard[k];
                        strcpy(tmp, tableau[k]);
                        retard[k] = retard[k+1];
                        strcpy(tableau[k],tableau[k+1]);
                        retard[k+1] = tmp2;
                        strcpy(tableau[k+1], tmp);
                    }
                }
            }
        }
    }
    
    printf("\n");
    printf("Les 5 compagnies aeriennes qui ont, en moyennes, le plus de retard sont : \n");
    printf("\n");
    printf("%s avec un retard moyen de %d min\n",tableau[4], retard[4]);
    printf("%s avec un retard moyen de %d min\n",tableau[3], retard[3]);
    printf("%s avec un retard moyen de %d min\n",tableau[2], retard[2]);
    printf("%s avec un retard moyen de %d min\n",tableau[1],retard[1]);
    printf("%s avec un retard moyen de %d min\n",tableau[0],retard[0]);

}

//Requete 6 : donne le retard moyen de la compagnie aérienne passée en paramètre
int delayed_airline(struct hachageairline h, char airlines[]){
    int air = ((int)airlines[0] + (int)airlines[1])-96;//Initialisation des variables
    if(strcmp("MQ", (airlines)) == 0){//On empeche la seule collision possible
        air++;
    }
    int somme = 0;
    int compteur = 0;
    struct pt_flight* vol = h.line[air];
    while(vol->nextcompany != NULL){//Tant qu'il y a des vols
        if(vol->vool.arr_delay >= 0){
            somme += vol->vool.arr_delay;//On additionne tous les retard et on utilise un compteur pour faire la moyenne
            compteur ++;
            vol = vol->nextcompany;
        }
        else{
            vol = vol->nextcompany;
        }
    }
    
    return somme/compteur;
}

//Requete 7 : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre
void most_delayed_airlines_at_airport(struct hachageairportsdep h, char airport[3]){
    int airp = (int)(airport[0])*3 + (int)(airport[1])*2 + (int)(airport[2])*1 -390;//Initialisation des variables
    struct pt_flight* vol = h.airportsdep[airp];
    char tableau[3][3] = {"  ", "  ", "  "};
    char tmp[3];
    int retard[3] = {0,0,0};
    int tmp2 = 0;
    if(vol == NULL){//On vérifie que l'aeroport selectionne a bien des vols de depart
        printf("L'aeroport selectionne n'a pas de vol de départ\n");
        printf("\n");
    }
    else{
        while(vol->nextairportdep != NULL){//On parcourt la liste
            if(retard[0] <= vol->vool.arr_delay){//On regarde si le retard d'arrivé max à l'aeroport est plus grand que celui dans le premier indice du tableau
                retard[0] = vol->vool.arr_delay;//Si oui alors on récupère les valeurs
                strcpy(tableau[0],vol->vool.airline); 
                for(int j = 0 ; j <= 2 ; j++){//Tri à bulle
                    for(int k = 0 ; k < 3 ; k++){
                        if(retard[k] >= retard[k+1]){
                            tmp2 = retard[k];
                            strcpy(tmp, tableau[k]);
                            retard[k] = retard[k+1];
                            strcpy(tableau[k],tableau[k+1]);
                            retard[k+1] = tmp2;
                            strcpy(tableau[k+1], tmp);
                        }
                    }
                }
            }
            vol = vol->nextairportdep;
        }   
        printf("\n");
        printf("Les 3 compagnies aeriennes qui ont le plus de retard d'arrivé à l'aéroport %s : \n", airport);
        printf("\n");
        printf("%s avec un retard de %d min\n",tableau[2], retard[2]);
        printf("%s avec un retard de %d min\n",tableau[1], retard[1]);
        printf("%s avec un retard de %d min\n",tableau[0], retard[0]);
    }
}

//Requete 8 : les vols annulés ou déviés à la date <date> (format M-D)
void changed_flights(struct hachagedate h, struct date d){
    int jour = ((d.month)-1)*31 +(d.day);//Initialisation des variables
    struct pt_flight* vol = h.jourmois[jour];
    printf("\n");
    printf("Les vols annulés ou déviés à la date %d/%d sont : \n", d.day, d.month);
    printf("\n");
    while(vol->nextdate != NULL){//On parcourt la liste
        if(vol->vool.diverted == 1 || vol->vool.cancelled == 1){//Si on tombe sur un vol annulé ou dévié alors on l'affiche
            printf("%s, %s, %s, %d, %f, %f, %d, %d, %f, %d, %d\n", vol->vool.airline, vol->vool.org_air, vol->vool.dest_air, vol->vool.sche_dep, vol->vool.dep_delay, vol->vool.air_time, vol->vool.dist, vol->vool.sched_arr, vol->vool.arr_delay, vol->vool.cancelled, vol->vool.diverted);

            vol = vol->nextdate;
        }
        else{
            vol = vol->nextdate;
        }
    }
}

//requete 9 : calcule le temps de vol moyen entre deux aéroports
void avg_flights_duration(struct hachageairportsdep h, char airport1[], char airport2[]){
  int hashairportdep = (int)(airport1[0])*3 + (int)(airport1[1])*2 + (int)(airport1[2])*1 -390;//Initialisation des variables
  int cpt = 0;
  float tmptotal = 0;
  struct pt_flight* tmp = h.airportsdep[hashairportdep];
  while (tmp != NULL){
    if ( (strcmp(tmp->vool.org_air, airport1) == 0) && (strcmp(tmp->vool.dest_air, airport2) == 0)){
      cpt++;
      tmptotal = tmptotal + tmp->vool.air_time;
    }
    tmp = tmp->nextairportdep;
  }

  if ( cpt != 0 ){
    tmptotal = tmptotal/cpt;
    printf("\n");
    printf("Le temps moyen pour aller de %s à %s est de %f min\n", airport1, airport2, tmptotal);
  }
  else{
    printf("\n");
    printf("Il n'y a pas de vols de réalisés entre %s et %s\n", airport1, airport2);
  }
}

//Requete 10 : trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnel, requête peut être limité à `xx` propositions, il peut y avoir des escales)
void find_itinerary(struct hachagedate h, struct date d, char airport1[3], char airport2[3]){
    int jour = ((d.month)-1)*31 +(d.day);
    struct pt_flight* vol = h.jourmois[jour];
    printf("Les itinéraires possibles entre l'aéroport %s et %s à la date du %d/%d sont :\n", airport1, airport2, d.day, d.month);
    while(vol->nextdate != NULL){
        if(((strcmp(vol->vool.org_air, airport1) == 0) && (strcmp(vol->vool.dest_air, airport2) == 0)) || ((strcmp(vol->vool.dest_air, airport1) == 0) && (strcmp(vol->vool.org_air, airport2) == 0))){
            printf("%s, %s, %s, %d, %f, %f, %d, %d, %f\n", vol->vool.airline, vol->vool.org_air, vol->vool.dest_air, vol->vool.sche_dep, vol->vool.dep_delay, vol->vool.air_time, vol->vool.dist, vol->vool.sched_arr, vol->vool.arr_delay);
            vol = vol->nextdate;
        }
        else{
            vol = vol->nextdate;
        }
    }
}


